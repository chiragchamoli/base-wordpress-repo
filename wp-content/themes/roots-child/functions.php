<?php
/**
 * Roots child custom functions
 *
 * this is loaded in addition to the roots functions.php file and does not overwrite
 */

require_once locate_template('/lib/child-scripts.php');          // Custom functions

?>