jQuery(document).ready(function($) {

	var RootsApp = new rootsApp().constructor();		
});

/**
* roots library 
* 
* @author Andrew McLagan <andrewmclagan@gmail.com>
* @package roots
*/
var rootsApp = function() {

	var self = this; // closure reference

	/**
	* class constructor
	*
	* @return null
	* @access public
	*/
	this.constructor = function() {

		// event handlers
		$('.overlay-container').on('hover click',this.productImageHover);	

		return this; // return
	}

	/**
	* product image hover
	*
	* @return null
	* @access public
	*/
	this.productImageHover = function(e) { 

		var $overlayNode = $(this);
		var $primaryImgNode = $overlayNode.find('.image.primary');
		var $secondaryImgNode = $overlayNode.find('.image.secondary');
		var timeoutDuration = 200;
		
		if(e.type == "mouseenter") {
			clearTimeout(self.piTimeoutId); // clear the timer
				
			self.timeoutId = setTimeout(function(){
			
				if ($secondaryImgNode.hasClass('secondary')){
					$primaryImgNode.stop(true,false).animate({ opacity: 0 }, timeoutDuration);
				}
			}, timeoutDuration);
			$overlayNode.children('.overlay').stop(true,true).slideDown('fast');
			
		} 
		else if(e.type == "mouseleave") {
		
			clearTimeout(self.piTimeoutId); // clear the timer
			
			self.timeoutId = setTimeout(function(){
					
				$primaryImgNode.stop(true,false).animate({ opacity: 1 }, timeoutDuration);
				
			}, timeoutDuration);
			
			$overlayNode.children('.overlay').stop(true,true).slideUp('fast');
		} 
		else if(e.type == "click") {
		
		    if( ! $overlayNode.children('.overlay').is(':visible') ) { // why?????
		    	
		    	e.preventDefault(); 
		    }
		}
	}

	/** 
	 * initPreviousSearch
	 * 
	 */		
	this.supportsStoreage = function() {
	
		try {
		  	return 'localStorage' in window && window['localStorage'] !== null;
		} catch (e) {
		  	return false;
		}
	}	
	
	/** 
	 * storeValue
	 * store the value in HTML 5 Storage API or cookies depending on support
	 */		
	this.storeValue = function(key,value) {
	
		// HTML5 Storage API
		if( localStorage = self.supportsStoreage() ) {
		
			return localStorage.setItem(key, JSON.stringify(value));
		}
		// Cookie Storage
		else if( !self.supportsStoreage() ) {
		
			return $.cookie(key, JSON.stringify(value));
		}
		
		return false; // no storage availible
	}	
	
	/** 
	 * getValue
	 * get a value from appropriate storage
	 */		
	this.getValue = function(key) {
	
		var value = false;
	
		if( self.supportsStoreage() ) {
		
			if( value = JSON.parse(localStorage.getItem(key)) )
				return value;
		}	
		else if( !self.supportsStoreage() ) {
		
			if( value = JSON.parse($.cookie(key)) ) 
				return value;
		}
		
		return false; // no value availible
	}		
}