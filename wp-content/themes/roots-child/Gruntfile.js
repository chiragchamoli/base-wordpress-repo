'use strict';
module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'assets/js/*.js',
        '../roots/assets/js/*.js',
        '!assets/js/*.min.js',
        '!../roots/assets/js/*.min.js'
      ]
    },
    recess: {
      dist: {
        options: {
          compile: true,
          compress: true
        },
        files: {
          'assets/css/app.min.css': [
            'assets/less/app.less',
            '../roots/assets/less/app.less',
            '../roots/assets/less/bootstrap/bootstrap-responsive.less',
            '../roots/assets/less/bootstrap/bootstrap.less'
          ]
        }
      }
    },
    concat: {
        options: {
            separator: ';'
        },      
        dist: {
            src: [
              'assets/js/app.js',
              '../roots/assets/js/vendor/jquery-1.10.1.js',
              '../roots/assets/js/vendor/bootstrap.js'
            ],
            dest: 'assets/js/app.min.js'
        }
    },    
    uglify: {
      dist: {
        files: {
          'assets/js/app.min.js': [
            'assets/js/app.min.js',
          ]
        }
      }
    },
    watch: {
      less: {
        files: [
          'assets/less/*.less',
          '../roots/assets/less/*.less',
          '../roots/assets/less/bootstrap/*.less'
        ],
        tasks: ['recess']
      },
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['jshint', 'uglify']
      }
    },
    clean: {
      dist: [
        'assets/css/app.min.css'
      ]
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-recess');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'recess',
    'concat',
    'uglify',
    'watch'
  ]);

};